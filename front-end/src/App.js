import Navbar from "./composants/Navbar";
import Home from "./composants/Home";

import { Route, Routes } from "react-router";
import { BrowserRouter } from "react-router-dom";
import Show from "./composants/show";
import Create from "./composants/Create";
import List from "./composants/List";
import Edit from "./composants/Edit";
import { useEffect } from "react";
import { getStagiaires } from "./features/ActionThunk";
import { useDispatch, useSelector } from "react-redux";

export default function App() {
  const dispatch = useDispatch();
  const edited = useSelector((state) => state.stg.edited);
  useEffect(() => {
    dispatch(getStagiaires());
  }, [edited]);
  return (
    <div className="app">
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/create" element={<Create />}></Route>
          <Route path="/list" element={<List />}></Route>
          <Route path="/stagiaire/:id" element={<Show />}></Route>
          <Route path="/stagiaire/:id/edit" element={<Edit />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}
