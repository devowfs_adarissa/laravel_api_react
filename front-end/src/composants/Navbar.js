import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { setTexteRecheche } from "../features/StagiaireSlice";

export default function NavBar() {
  const stagiaires = useSelector((state) => state.stg.stagiaires);
  const dispatch = useDispatch();
  function handleChange(e) {
    dispatch(setTexteRecheche(e.target.value));
  }
  return (
    <nav className="navbar navbar-expand-lg bg-body-tertiary">
      <div className="container px-5">
        <a className="navbar-brand" href="#">
          Redux CRUD APP
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0 px-5">
            <li className="nav-item">
              <Link className="nav-link" aria-current="page" to="/">
                Home
              </Link>
            </li>
            {/* <li className="nav-item">
              <Link className="nav-link" to="/create">
                Nouveau stagiaire
              </Link>
            </li> */}
            <li className="nav-item">
              <Link className="nav-link" to="/list">
                Tous les stagiaires{" "}
                <span>{stagiaires && `(${stagiaires.length})`}</span>
              </Link>
            </li>
          </ul>
          <input
            className="form-control me-2"
            style={{ width: "300px" }}
            type="search"
            placeholder="Chercher par nom"
            aria-label="Search"
            onChange={handleChange}
          />
        </div>
      </div>
    </nav>
  );
}
