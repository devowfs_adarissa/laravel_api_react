import { useDispatch, useSelector } from "react-redux";
import { deleteStagiaire } from "../features/ActionThunk";
import { Link } from "react-router-dom";

export default function List() {
  const stagiaires = useSelector((state) => state.stg.stagiaires);
  const text_recherche = useSelector((state) => state.stg.text_recherche);

  const dispatch = useDispatch();

  function handleSupprimer(stg) {
    if (window.confirm(`Voulez vous supprimer le stagiaire?${stg.nom_complet}`))
      dispatch(deleteStagiaire(stg.id));
  }
  return (
    <>
      <div className="container mt-5">
        <Link to="/create" className="btn btn-primary">
          Nouveau stagiaire
        </Link>
      </div>
      <div className="container text-center mt-4">
        {stagiaires && (
          <div>
            <h3 className="mb-5">Tous les stagiaires</h3>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nom complet</th>
                  <th>Genre</th>
                  <th>Date de naissance</th>
                  <th>Note</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {stagiaires.filter((stg)=>{
                  if(text_recherche === ""){return stg}
                  return stg.nom_complet.toLowerCase().includes(text_recherche.toLowerCase());
                }).map((stg) => (
                  <tr>
                    <td>{stg.id}</td>
                    <td>{stg.nom_complet}</td>
                    <td>{stg.genre == "M" ? "Male" : "Femelle"}</td>
                    <td>{stg.date_naissance}</td>
                    <td>{stg.note}</td>
                    <td>
                      <Link
                        to={`/stagiaire/${stg.id}`}
                        className="btn btn-info"
                      >
                        Détails
                      </Link>
                      <Link
                        to={`/stagiaire/${stg.id}/edit`}
                        className="btn btn-info"
                      >
                        Edit
                      </Link>
                      <button
                        onClick={() => handleSupprimer(stg)}
                        className="btn btn-danger"
                      >
                        Supprimer
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </div>
    </>
  );
}
