import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getStagiaires, updateStagiaire } from "../features/ActionThunk";
import { useNavigate, useParams } from "react-router";

export default function Edit() {
  const [stg, setStg] = useState(null);
  const stagiaires = useSelector((state) => state.stg.stagiaires);
  const { id } = useParams();
  //   useEffect(() => {
  //     if (!stagiaires) dispatch(getStagiaires);
  //   }, []);

  useEffect(() => {
    const trv = stagiaires.find((stg) => stg.id == id);
    if (trv) setStg(trv);
  }, [stagiaires]);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  function handleChange(e) {
    setStg({ ...stg, [e.target.name]: e.target.value });
  }
  function handleSubmit(e) {
    e.preventDefault();
    dispatch(updateStagiaire(stg));
    navigate("/list");
  }
  return stg ? (
    <div>
      <h3 className="mt-4 mb-4 text-center">Ajouter un nouveau stagiaire</h3>
      <form className="w-50 mx-auto mt-4" onSubmit={handleSubmit}>
        <div className="mb-3">
          <label for="nom_complet" className="form-label">
            Nom complet
          </label>
          <input
            type="text"
            className="form-control"
            id="nom_complet"
            name="nom_complet"
            value={stg.nom_complet}
            onChange={handleChange}
          />
        </div>
        <div className="mb-3">
          <label for="date_naissance" className="form-label">
            Date de naissance
          </label>
          <input
            type="date"
            className="form-control"
            id="date_naissance"
            name="date_naissance"
            value={stg.date_naissance}
            onChange={handleChange}
          />
        </div>

        <div className="mb-3">
          <label htmlFor="genre" className="form-label">
            Genre
          </label>
          <div>
            <input
              type="radio"
              name="genre"
              id="m"
              value="m"
              onChange={handleChange}
              checked={stg.genre === "M" ? "checked" : ""}
            />{" "}
            <span>Male</span>
            <input
              type="radio"
              name="genre"
              className="mx-3"
              id="f"
              value="f"
              onChange={handleChange}
              checked={stg.genre === "F" ? "checked" : ""}
            />
            <span>Femelle</span>
          </div>
        </div>
        <div className="mb-3">
          <label for="note" className="form-label">
            Note
          </label>
          <input
            type="text"
            className="form-control"
            id="note"
            name="note"
            value={stg.note}
            onChange={handleChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Ajouter
        </button>
      </form>
    </div>
  ) : (
    <p>Aucun stagiaire n'est trouvé!</p>
  );
}
