import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { showStagiaire } from "../features/ActionThunk";

export default function Show() {
  const { id } = useParams();
  const stagiaire = useSelector((state) => state.stg.details_stagiaire);
  const disptach = useDispatch();

  useEffect(() => {
    disptach(showStagiaire(id));
  }, [id]);
  return (
    stagiaire && (
      <div className="card w-75 mx-auto mt-4">
        <h5 className="card-header bg-primary text-white fw-bold">
          {stagiaire.nom_complet}
        </h5>
        <div className="card-body">
          <p>Date de naissance: {stagiaire.date_naissance}</p>
          <p>Genre: {stagiaire.genre == "M" ? "Male" : "Femelle"}</p>
          <p>Note: {stagiaire.note}</p>
        </div>
      </div>
    )
  );
}
