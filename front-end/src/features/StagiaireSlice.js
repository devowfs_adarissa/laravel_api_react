import { createSlice } from "@reduxjs/toolkit";
import {
  createStagiaire,
  deleteStagiaire,
  getStagiaires,
  showStagiaire,
  updateStagiaire,
} from "./ActionThunk";

const StagiaireSlice = createSlice({
  name: "stagiaireSlice",
  initialState: {
    loading: false,
    error: null,
    stagiaires: [],
    details_stagiaire: null,
    edited: false, //Variable permettant de savoir est ce qu'il y a un changement de la liste des stagiaires au niveau de l'API ou non
    text_recherche: "",
  },
  reducers: {
    setTexteRecheche: (state, action) => {
      state.text_recherche = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getStagiaires.pending, (state) => {
        console.log("pending API call");
        state.edited = false;
        state.loading = true;
      })
      .addCase(getStagiaires.rejected, (state, action) => {
        state.loading = false;
        console.log("rejected ".action);
        // state.error = action.payload.message;
      })
      .addCase(getStagiaires.fulfilled, (state, action) => {
        state.loading = false;
       
        state.stagiaires = action.payload;
      })
      .addCase(showStagiaire.pending, (state) => {
        state.loading = true;
      })
      .addCase(showStagiaire.rejected, (state, action) => {
        state.loading = false;
        console.log(action.payload);
      })
      .addCase(showStagiaire.fulfilled, (state, action) => {
        state.details_stagiaire = action.payload;
        state.loading = false;
        state.edited = true;
      })
      .addCase(createStagiaire.pending, (state) => {
        state.loading = true;
         state.edited = false;
      })
      .addCase(createStagiaire.rejected, (state, action) => {
        state.loading = false;
        // state.error = action.payload.message;
      })
      .addCase(createStagiaire.fulfilled, (state, action) => {
        state.loading = false;
        state.edited = true;
      })
      .addCase(updateStagiaire.pending, (state) => {
        state.edited = false;
        state.loading = true;
      })
      .addCase(updateStagiaire.rejected, (state, action) => {
        state.loading = false;
        state.edited = false;
        state.error = action.payload.message;
      })
      .addCase(updateStagiaire.fulfilled, (state, action) => {
        state.loading = false;
        state.edited = true;
      })
      .addCase(deleteStagiaire.pending, (state) => {
        state.loading = true;
        state.edited = false;
      })
      .addCase(deleteStagiaire.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload.message;
        state.edited = false;
      })
      .addCase(deleteStagiaire.fulfilled, (state, action) => {
        state.loading = false;
        state.edited = true;
      });
  },
});
export const { setTexteRecheche } = StagiaireSlice.actions;
export default StagiaireSlice.reducer;
