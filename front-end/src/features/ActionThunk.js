import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const getStagiaires = createAsyncThunk("stg/get", async () => {
  return axios
    .get("http://127.0.0.1:8000/api/stagiaire")
    .then((response) => response.data);
});
const showStagiaire = createAsyncThunk("stg/show", async (id) => {
  return axios
    .get(`http://127.0.0.1:8000/api/stagiaire/${id}`)
    .then((response) => response.data);
});

const createStagiaire = createAsyncThunk("stg/create", async (nvstg) => {
  return axios
    .post("http://127.0.0.1:8000/api/stagiaire", nvstg)
    .then((response) => response.data);
});

const updateStagiaire = createAsyncThunk("stg/update", async (stg) => {
  return axios
    .put(`http://127.0.0.1:8000/api/stagiaire/${stg.id}`, stg)
    .then((response) => response.data);
});

const deleteStagiaire = createAsyncThunk("stg/delete", async (id) => {
  return axios
    .delete(`http://127.0.0.1:8000/api/stagiaire/${id}`)
    .then((response) => response.data);
});

export {
  getStagiaires,
  createStagiaire,
  updateStagiaire,
  deleteStagiaire,
  showStagiaire,
};
