<?php

namespace App\Http\Controllers;

use App\Models\Stagiaire;
use Illuminate\Http\Request;

class StagiaireController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()   {   
       $stagiaires= Stagiaire::all();
      return response()->json($stagiaires,201);
       }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)  {   
       $nv_stagiaire= Stagiaire::create($request->all());
       return  response()->json($nv_stagiaire,201);
     }
    /**
     * Display the specified resource.
     */
    public function show(Stagiaire $stagiaire)   { 
        return  response()->json($stagiaire,201);
        }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Stagiaire $stagiaire)   { 
        $stagiaire ->update($request->all());
        return  response()->json($stagiaire,201);
       }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Stagiaire $stagiaire)   {   
    
        $stagiaire->delete();
        // On retourne la réponse JSON
        return response()->json();
         
      }
}
